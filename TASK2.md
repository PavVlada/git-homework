### TASK 2
1.10. Была вызвана команда `diff`
````
jaabi484@K501UQ:~/devops/git-homework$ git diff
diff --git a/TASK1.md b/TASK1.md
index f6e102b..c75441d 100644
--- a/TASK1.md
+++ b/TASK1.md
@@ -17,3 +17,41 @@ jaabi484@K501UQ:~/devops/git-homework$ git init --initial-branch=main
 jaabi484@K501UQ:~/devops/git-homework$ git remote add origin git@gitlab.com:PavVlada/git-homework.git
 ```
 6. В этом шаге был создан файл TASK1.md
+7. Были добавлены все имеющиеся файлы в локальном репозитории в staging area
+```
+jaabi484@K501UQ:~/devops/git-homework$ git add .
+```
+Проверка статуса гита
+```
+jaabi484@K501UQ:~/devops/git-homework$ git status
+На ветке main
+
+Еще нет коммитов
+
+Изменения, которые будут включены в коммит:
+  (используйте «git rm --cached <файл>…», чтобы убрать из индекса)
+       новый файл:    TASK1.md
+
+
+```
+Был совершен коммит файла в локальный репозиторий
+```
+jaabi484@K501UQ:~/devops/git-homework$ git commit -m "Initial commit"
+[main (корневой коммит) 09bddfd] Initial commit
+ 1 file changed, 19 insertions(+)
+ create mode 100644 TASK1.md
+```
+8. Был сделан push в удаленный репозиторий
+```
+jaabi484@K501UQ:~/devops/git-homework$ git push -u origin main
+Перечисление объектов: 3, готово.
+Подсчет объектов: 100% (3/3), готово.
+При сжатии изменений используется до 4 потоков
+Сжатие объектов: 100% (2/2), готово.
+Запись объектов: 100% (3/3), 679 байтов | 339.00 КиБ/с, готово.
+Всего 3 (изменений 0), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
+To gitlab.com:PavVlada/git-homework.git
+ * [new branch]      main -> main
+branch 'main' set up to track 'origin/main'.
+```
+9. Были дописаны шаги в TASK1.md
````
Была вызвана команда `status`
```
jaabi484@K501UQ:~/devops/git-homework$ git status
На ветке main
Ваша ветка обновлена в соответствии с «origin/main».

Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
	изменено:      TASK1.md

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)
```
1.11. Были закоммичены изменения
```
jaabi484@K501UQ:~/devops/git-homework$ git add TASK1.md
jaabi484@K501UQ:~/devops/git-homework$ git commit -m "Added steps in TASK1.md"
[main 6223af9] Added steps in TASK1.md
 1 file changed, 38 insertions(+)
```
1.12. Была выполнена команда `push` в удаленный репозиторий
```
jaabi484@K501UQ:~/devops/git-homework$ git push
Перечисление объектов: 5, готово.
Подсчет объектов: 100% (5/5), готово.
При сжатии изменений используется до 4 потоков
Сжатие объектов: 100% (2/2), готово.
Запись объектов: 100% (3/3), 1.30 КиБ | 666.00 КиБ/с, готово.
Всего 3 (изменений 0), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
To gitlab.com:PavVlada/git-homework.git
   09bddfd..6223af9  main -> main
```
2. Был локально отведен бранч `develop`:
```
jaabi484@K501UQ:~/devops/git-homework$ git branch develop
```
Был удаленно отведен бранч `develop`
```
jaabi484@K501UQ:~/devops/git-homework$ git push --set-upstream origin develop
Всего 0 (изменений 0), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
remote: 
remote: To create a merge request for develop, visit:
remote:   https://gitlab.com/PavVlada/git-homework/-/merge_requests/new?merge_request%5Bsource_branch%5D=develop
remote: 
To gitlab.com:PavVlada/git-homework.git
 * [new branch]      develop -> develop
branch 'develop' set up to track 'origin/develop'.
```
Переключение на ветку develop
```
jaabi484@K501UQ:~/devops/git-homework$ git checkout develop 
M	TASK1.md
Переключено на ветку «develop»
```
3. Была добавлена в конец файла TASK1.MD строка "Задание провалено!:(" и создан файл TASK2.md.
Были залиты изменения локально и удаленно
```
jaabi484@K501UQ:~/devops/git-homework$ git add .
jaabi484@K501UQ:~/devops/git-homework$ git status
На ветке develop
Ваша ветка обновлена в соответствии с «origin/develop».

Изменения, которые будут включены в коммит:
  (используйте «git restore --staged <файл>…», чтобы убрать из индекса)
	изменено:      TASK1.md
	новый файл:    TASK2.md

jaabi484@K501UQ:~/devops/git-homework$ git commit -m "Added TASK2.md and changed TASK1.md"
[develop 6bcbd11] Added TASK2.md and changed TASK1.md
 2 files changed, 87 insertions(+)
 create mode 100644 TASK2.md
jaabi484@K501UQ:~/devops/git-homework$ git push
Перечисление объектов: 6, готово.
Подсчет объектов: 100% (6/6), готово.
При сжатии изменений используется до 4 потоков
Сжатие объектов: 100% (4/4), готово.
Запись объектов: 100% (4/4), 1.62 КиБ | 553.00 КиБ/с, готово.
Всего 4 (изменений 1), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
remote: 
remote: To create a merge request for develop, visit:
remote:   https://gitlab.com/PavVlada/git-homework/-/merge_requests/new?merge_request%5Bsource_branch%5D=develop
remote: 
To gitlab.com:PavVlada/git-homework.git
   6223af9..6bcbd11  develop -> develop
```
4. Был переключен бранч на main
```
jaabi484@K501UQ:~/devops/git-homework$ git checkout main
Переключено на ветку «main»
Ваша ветка обновлена в соответствии с «origin/main».
```
5. Была добавлена в файле TASK1.md строка "Задание успешно выполнено!:)". Были залиты изменения локально и удаленно
```
jaabi484@K501UQ:~/devops/git-homework$ git add .
jaabi484@K501UQ:~/devops/git-homework$ git commit -m "TASK1.md is changed"
[main 243c569] TASK1.md is changed
 1 file changed, 2 insertions(+)
jaabi484@K501UQ:~/devops/git-homework$ git push
Перечисление объектов: 5, готово.
Подсчет объектов: 100% (5/5), готово.
При сжатии изменений используется до 4 потоков
Сжатие объектов: 100% (2/2), готово.
Запись объектов: 100% (3/3), 329 байтов | 109.00 КиБ/с, готово.
Всего 3 (изменений 1), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
To gitlab.com:PavVlada/git-homework.git
   6223af9..243c569  main -> main
```
6. Был выполнен мерж из ветки `develop` в основную верку и разрешен конфликт в пользу основной ветки. 
В итоге в бранче main появился файл TASK2.md, но вместо строки "Задание провалено!:(" появилась строка "Задание успешно выполнено!:)"
7. Логи в ветке main
```
jaabi484@K501UQ:~/devops/git-homework$ git log
commit 243c5698d75839451602305ff1d281eda23a2e74 (HEAD -> main, origin/main)
Author: PavVlada <pav.vlada888@gmail.com>
Date:   Mon Apr 4 01:08:42 2022 +0300

    TASK1.md is changed

commit 6223af9eb079a7085cf6996a46eda905cac74e78
Author: PavVlada <pav.vlada888@gmail.com>
Date:   Mon Apr 4 00:23:22 2022 +0300

    Added steps in TASK1.md

commit 09bddfdcc62e640a346f743751a48096c9d4e18c
Author: PavVlada <pav.vlada888@gmail.com>
Date:   Sun Apr 3 23:58:05 2022 +0300

    Initial commit
```
Логи в ветке develop
```
commit 6bcbd11adfc4092956f569f4e2e02d6ef3530c38 (HEAD -> develop, origin/develop)
Author: PavVlada <pav.vlada888@gmail.com>
Date:   Mon Apr 4 01:02:28 2022 +0300

    Added TASK2.md and changed TASK1.md

commit 6223af9eb079a7085cf6996a46eda905cac74e78
Author: PavVlada <pav.vlada888@gmail.com>
Date:   Mon Apr 4 00:23:22 2022 +0300

    Added steps in TASK1.md

commit 09bddfdcc62e640a346f743751a48096c9d4e18c
Author: PavVlada <pav.vlada888@gmail.com>
Date:   Sun Apr 3 23:58:05 2022 +0300

    Initial commit
```
8. Были описаны проделанные шаги в TASK2.MD

PS Я буду делать задание 3
