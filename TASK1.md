### TASK 1
1. Был взят уже имеющийся аккаунт на gitlab
2. Был создан новый удаленный репозиторий
3. Git был установлен ранее, версия 2.35.1

```
jaabi484@K501UQ:~/devops/git-homework$ git --version
git version 2.35.1
```
4. Был инициализирован локальный репозиторий с помощью команды `git init`:
```
jaabi484@K501UQ:~/devops/git-homework$ git init --initial-branch=main
Инициализирован пустой репозиторий Git в /home/jaabi484/devops/git-homework/.git/
```
5. Был связан локальный репозиторий с удаленным, но пока не была выполнена синхронизация.
```
jaabi484@K501UQ:~/devops/git-homework$ git remote add origin git@gitlab.com:PavVlada/git-homework.git
```
6. В этом шаге был создан файл TASK1.md
7. Были добавлены все имеющиеся файлы в локальном репозитории в staging area
```
jaabi484@K501UQ:~/devops/git-homework$ git add .
```
Проверка статуса гита
```
jaabi484@K501UQ:~/devops/git-homework$ git status
На ветке main

Еще нет коммитов

Изменения, которые будут включены в коммит:
  (используйте «git rm --cached <файл>…», чтобы убрать из индекса)
	новый файл:    TASK1.md


```
Был совершен коммит файла в локальный репозиторий
```
jaabi484@K501UQ:~/devops/git-homework$ git commit -m "Initial commit"
[main (корневой коммит) 09bddfd] Initial commit
 1 file changed, 19 insertions(+)
 create mode 100644 TASK1.md
```
8. Был сделан push в удаленный репозиторий
```
jaabi484@K501UQ:~/devops/git-homework$ git push -u origin main
Перечисление объектов: 3, готово.
Подсчет объектов: 100% (3/3), готово.
При сжатии изменений используется до 4 потоков
Сжатие объектов: 100% (2/2), готово.
Запись объектов: 100% (3/3), 679 байтов | 339.00 КиБ/с, готово.
Всего 3 (изменений 0), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
To gitlab.com:PavVlada/git-homework.git
 * [new branch]      main -> main
branch 'main' set up to track 'origin/main'.
```
9. Были дописаны шаги в TASK1.md

Задание успешно выполнено!:)
