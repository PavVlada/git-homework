### TASK 3
1. Была изучена команда `git stash`
2. Был переключен бранч на main. Проверка с помощью `git status`
```
jaabi484@K501UQ:~/devops/git-homework$ git status
На ветке main
Ваша ветка обновлена в соответствии с «origin/main».

нечего коммитить, нет изменений в рабочем каталоге
```
3. В TASK2.MD была дописана строка "PS Я буду делать задание 3"
4. Были добавлены изменения в staging area
```
jaabi484@K501UQ:~/devops/git-homework$ git add TASK2.md 
```
5. Был вызван `git stash`
```
jaabi484@K501UQ:~/devops/git-homework$ git stash
Рабочий каталог и состояние индекса сохранены WIP on main: 0cf9276 Added steps in TASK2.md
```
6. Был вызван `git stash list`
```
jaabi484@K501UQ:~/devops/git-homework$ git stash list
stash@{0}: WIP on main: 0cf9276 Added steps in TASK2.md
```
Был вызван `git status`
```
jaabi484@K501UQ:~/devops/git-homework$ git status
На ветке main
Ваша ветка обновлена в соответствии с «origin/main».

нечего коммитить, нет изменений в рабочем каталоге
```
7. Была переключена ветка на develop
```
jaabi484@K501UQ:~/devops/git-homework$ git checkout develop 
Переключено на ветку «develop»
Ваша ветка отстает от «origin/develop» на 2 коммита и может быть перемотана вперед.
  (используйте «git pull», чтобы обновить вашу локальную ветку)
```
8. В TASK2.MD нет изменений. Нет строки "PS Я буду делать задание 3". 
9. Был добавлен файл TASK3.MD с описанием проделанной работы
10. Были закоммичены изменения
```
jaabi484@K501UQ:~/devops/git-homework$ git add .
jaabi484@K501UQ:~/devops/git-homework$ git status
На ветке develop
Ваша ветка отстает от «origin/develop» на 2 коммита и может быть перемотана вперед.
  (используйте «git pull», чтобы обновить вашу локальную ветку)

Изменения, которые будут включены в коммит:
  (используйте «git restore --staged <файл>…», чтобы убрать из индекса)
	новый файл:    TASK3.md

jaabi484@K501UQ:~/devops/git-homework$ git commit -m "Added TASK3.md"
[develop 0869698] Added TASK3.md
 1 file changed, 42 insertions(+)
 create mode 100644 TASK3.md

jaabi484@K501UQ:~/devops/git-homework$ git push
Перечисление объектов: 4, готово.
Подсчет объектов: 100% (4/4), готово.
При сжатии изменений используется до 4 потоков
Сжатие объектов: 100% (3/3), готово.
Запись объектов: 100% (3/3), 1013 байтов | 506.00 КиБ/с, готово.
Всего 3 (изменений 0), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
remote: 
remote: To create a merge request for develop, visit:
remote:   https://gitlab.com/PavVlada/git-homework/-/merge_requests/new?merge_request%5Bsource_branch%5D=develop
remote: 
To gitlab.com:PavVlada/git-homework.git
   44a803f..0869698  develop -> develop

```
11. Была переключена ветка на main
```
jaabi484@K501UQ:~/devops/git-homework$ git checkout main
Переключено на ветку «main»
Ваша ветка обновлена в соответствии с «origin/main».

```
12. Вернуть stash изменения TASK2.MD назад
Перед возвращением изменений была выполнена команда `git stash list`
```
jaabi484@K501UQ:~/devops/git-homework$ git stash list
stash@{0}: WIP on main: 0cf9276 Added steps in TASK2.md

```
И `git status`
```
jaabi484@K501UQ:~/devops/git-homework$ git status
На ветке main
Ваша ветка обновлена в соответствии с «origin/main».

нечего коммитить, нет изменений в рабочем каталоге
```
Строки "PS Я буду делать задание 3" нет.
Была выполнена команда `git stash apply`
```
На ветке main
Ваша ветка обновлена в соответствии с «origin/main».

Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git restore <файл>…», чтобы отменить изменения в рабочем каталоге)
	изменено:      TASK2.md

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)
```
Строка "PS Я буду делать задание 3" появилась.
13. Были закоммичены изменения
```
jaabi484@K501UQ:~/devops/git-homework$ git add .
jaabi484@K501UQ:~/devops/git-homework$ git commit -m "Added last line in TASK2.md"
[main feb48a1] Added last line in TASK2.md
 1 file changed, 2 insertions(+)
jaabi484@K501UQ:~/devops/git-homework$ git push
Перечисление объектов: 5, готово.
Подсчет объектов: 100% (5/5), готово.
При сжатии изменений используется до 4 потоков
Сжатие объектов: 100% (3/3), готово.
Запись объектов: 100% (3/3), 363 байта | 363.00 КиБ/с, готово.
Всего 3 (изменений 1), повторно использовано 0 (изменений 0), повторно использовано пакетов 0
To gitlab.com:PavVlada/git-homework.git
   0cf9276..feb48a1  main -> main
```
14. Был смержен develop в main
15. Были дописаны в TASK3.md проделанные шаги

Хочу отметить, что в 7 пункте в ветке develop еще не был произведен `git pull origin main`, но после это было сделано (разумеется, когда еще не была закоммичена строка "PS Я буду делать задание 3")
